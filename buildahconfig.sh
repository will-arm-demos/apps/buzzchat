#!/bin/bash 

# Set configurations for the container
sudo buildah config --author "Will Christensen wirewc@daotechnologies.com" $mycontainer
sudo buildah config --comment "Bookdemo python container."  $mycontainer
